'use strict'

const OK = 200
const NOT_FOUND = 404
const reservationsTableBody = document.getElementById("reservations-table-body")

const searchStartDate = document.getElementById("searchInputDateStart")
const searchEndDate = document.getElementById("searchInputDateEnd")
const searchStartTime = document.getElementById("searchInputTimeStart")
const searchEndTime = document.getElementById("searchInputTimeEnd")

const addStartDate = document.getElementById("addInputDateStart")
const addEndDate = document.getElementById("addInputDateEnd")
const addStartTime = document.getElementById("addInputTimeStart")
const addEndTime = document.getElementById("addInputTimeEnd")
const addPcID = document.getElementById("addInputPcID")
const addInputProjectname = document.getElementById("addInputProjectName")
const remindUser = document.getElementById("remindUser")

async function checkLogin(form){
    const status = await login(form[0].value, form[1].value, form[3].checked)

    if (status)
        window.location.replace("")
    else
        alert("Login Failed")
}

async function logout(){
    storage.clear()
    window.location.replace("/login")
}

async function addReservationsOnClick() {
    const start = toUnix(addStartDate.value, addStartTime.value)
    const end = toUnix(addEndDate.value, addEndTime.value)

    await addReservation(start, end, addPcID.value, addInputProjectname.value, !remindUser.checked)
}

async function searchReservationsOnClick() {
    const from = toUnix(searchStartDate.value, searchStartTime.value)
    const to = toUnix(searchEndDate.value, searchEndTime.value)

    await searchReservations(from, to)
}

function toUnix(dateStr, timeStr) {
    const date = new Date(dateStr)
    const [hours, minutes] = timeStr.split(":").map(Number)

    date.setHours(hours, minutes)
    return Math.floor(date.getTime() / 1000)
}


async function addReservation(start, end, pc_id, project_name, is_user_reminded) {
    const status = await postReservation({
        start: start,
        end: end,
        pc_id: pc_id,
        project_name: project_name,
        is_user_reminded: is_user_reminded,
    })

    switch (status) {
        case OK:
            alert("Reservierung erfolgreich angelegt.")
            break
        case NOT_FOUND:
            alert("Das Projekt existiert nicht. Sie müssen das Projekt zuerst hochladen.")
            break
        default:
            alert("Etwas hat nicht geklappt.")
            break
    }
}

async function searchReservations(from = undefined, to = undefined) {
    const email = storage.getItem("email")
    const reservations = await getAllReservations(email, from, to)
    buildReservationsTable(reservations)
}

function buildReservationsTable(reservations) {
    reservationsTableBody.innerHTML = ""

    const sortedReservations = reservations.sort((r1, r2) => r1.start < r2.start)
    sortedReservations.forEach(({id, start, end, pc_id, project_name}) => {
        const row = reservationsTableBody.insertRow()
        const idCell = row.insertCell()
        const startDateTimeCell = row.insertCell()
        const endDateTimeCell = row.insertCell()
        const pcIdCell = row.insertCell()
        const projectNameCell = row.insertCell()
        const DeleteReservationCell = row.insertCell()

        idCell.innerHTML = id
        startDateTimeCell.appendChild(editableDate(new Date(start * 1000).toLocaleString()))
        endDateTimeCell.appendChild(editableDate(new Date(end * 1000).toLocaleString()))
        pcIdCell.innerHTML = pc_id
        projectNameCell.innerHTML = project_name
        DeleteReservationCell.appendChild(deleteReservationButton(id))
    })
}

function deleteReservationButton(id) {
    const deleteButton = document.createElement("input")
    deleteButton.setAttribute("type", "button")
    deleteButton.setAttribute("value", "X")
    deleteButton.setAttribute("style", "background-color:#A52A2A; border-color:#A52A2A; color:white")
    deleteButton.onclick = async function () {
        if(confirm("Wollen Sie die Reservierung mit der ID " + id + " wirklich löschen?")) {
            const result = await deleteReservation(id)
            if (result === OK)
                alert("Update war erfolgreich.")
            else
                alert("Etwas hat nicht geklappt.")
            await searchReservationsOnClick()
        }
    }
    return deleteButton
}

function editableDate(date) {
    const inputField = document.createElement("input")
    inputField.setAttribute("type", "text")
    inputField.value = date
    inputField.addEventListener("change", async () => {
        const {id, fromLocale, toLocale, pc_id, project_name} = getReservationFromRow(inputField)
        const from = localeToUnix(fromLocale)
        const to = localeToUnix(toLocale)

        const result = await updateReservation(id, from, to, pc_id, project_name)
        if (result === OK)
            alert("Update war erfolgreich.")
        else
            alert("Etwas hat nicht geklappt.")
    })
    return inputField
}

function getReservationFromRow(child) {
    const cellsHTML = child.parentNode.parentNode.childNodes
    return {
        id: cellsHTML.item(0).innerHTML,
        fromLocale: cellsHTML.item(1).childNodes.item(0).value,
        toLocale: cellsHTML.item(2).childNodes.item(0).value,
        pc_id: cellsHTML.item(3).innerHTML,
        project_name: cellsHTML.item(4).innerHTML
    }
}

async function updateReservation(id, from, to, pc_id, project_name) {
    const reservation = await getReservation(id)
    await deleteReservation(id)
    console.log(reservation)
    const res = await postReservation({
        start: from,
        end: to,
        pc_id: pc_id,
        project_name: project_name,
        is_user_reminded: reservation.is_user_reminded
    })
    await searchReservationsOnClick()
    return res
}

function localeToUnix(locale) {
    const [date, timeStr] = locale.split(" ")
    console.log({date, timeStr})
    const [day, month, year] = date.split(".")
    console.log({day, month, year})
    const dateStr = `${year.slice(0, -1)}-${month.padStart(2, "0")}-${day.padStart(2, "0")}`
    console.log({dateStr})
    console.log(toUnix(dateStr, timeStr));
    return toUnix(dateStr, timeStr)
}

async function uploadFilesFromPC(files) {
    return await uploadFiles(files)
}

async function deployFileFromStorage(remoteHost, projectName, startGNS) {
    let response1 = await deployFile(remoteHost, projectName)
    if(startGNS) {
        let response2 = await openInGNS(remoteHost, projectName)
        return [response1, response2]
    } else {
        return [response1]
    }
}