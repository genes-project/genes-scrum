const version = "v1"

let storage = sessionStorage

async function login(email, passwd, save) {
    let response = await fetch(`/${version}/login`, {
        method: "POST",
        body: JSON.stringify({email, passwd})
    })

    if(response.status === 200){
        let json = await response.json()

        if(save)
            storage = localStorage

        storage.setItem("token", json.jwt)
        storage.setItem("email", email)
        storage.setItem("role", json.role)

        return response.status
    }
    else {
        return null
    }
}

async function getAllReservations(email=undefined, from=undefined, to=undefined) {
    let url = `/${version}/reservations?`
    let params = {}

    if(email)
        params.email = email
    if(from)
        params.from = from
    if(to)
        params.to = to

    let response = await fetch(url + new URLSearchParams(params), {
        method: "GET",
        headers: {
            'Authorization': `Bearer ${storage.getItem("token")}`
        }
    })

    let json = await response.json()
    return json.map(model => {
        return {id: model.id, start: model.start, end: model.end, pc_id: model.pc_id, project_name: model.project_name}
    })
}

async function getReservation(id) {
    const url = `/${version}/reservations/${id}`
    const response = await fetch(url, {
        method: "GET",
        headers: {
            'Authorization': `Bearer ${storage.getItem("token")}`
        }
    })

    const model = await response.json()
    return {
        id: model.id,
        start: model.start,
        end: model.end,
        pc_id: model.pc_id,
        project_name: model.project_name,
        is_user_reminded: model.is_user_reminded
    }
}

async function patchReservation(id, model) {
    let response = await fetch(`/${version}/reservations/${id}`, buildRequestInfo("PATCH", model))
    return response.status
}

async function postReservation(model) {
    console.log(model)
    let response = await fetch(`/${version}/reservations/`, buildRequestInfo("POST", model))
    console.log(response)
    return response.status
}

async function deleteReservation(id) {
    let response = await fetch(`/${version}/reservations/${id}`, buildRequestInfo("DELETE", {
        method: "DELETE",
        headers: {
            'Authorization': `Bearer ${storage.getItem("token")}`
        }
    }))
    return response.status
}

function buildRequestInfo(method, model) {
    return {
        method: method,
        headers: {
            'Authorization': `Bearer ${storage.getItem("token")}`
        },
        body: JSON.stringify(model)
    }
}

async function uploadFiles(files) {
	const formData = new FormData();
	for (let i = 0; i < files.length; i++) {
		formData.append('files', files[i]);
	}
    let response = await fetch(`/${version}/prov/upload`, {
        method: "POST",
		headers: {
            'Authorization': `Bearer ${storage.getItem("token")}`
        },
        body: formData
    })
    return response.status
}

async function deployFile(remoteHost, projectName) {
    let params = {}
    params.remoteHost = remoteHost
    params.projectName = projectName

    let response = await fetch(`/${version}/prov/deploy?` + new URLSearchParams(params), {
        method: "GET",
        headers: {
            'Authorization': `Bearer ${storage.getItem("token")}`
        }
    })
    return response.status
}

async function openInGNS(remoteHost, projectName) {
    let params = {}
    params.remoteHost = remoteHost
    params.projectName = projectName

    let response = await fetch(`/${version}/prov/start?` + new URLSearchParams(params), {
        method: "GET",
        headers: {
            'Authorization': `Bearer ${storage.getItem("token")}`
        }
    })
    return response.status
}