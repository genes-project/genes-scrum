package org.genes.webserver.mail.apiadapter;

import org.genes.webserver.Database;
import org.genes.webserver.config.Config;
import org.genes.webserver.model.reservations.Reservation;
import org.javalite.activejdbc.LazyList;
import org.junit.jupiter.api.Test;

public class MailtrapAPIAdapterTest {

    @Test
    void sendMailToMailtrapTest() {
        Config config = Config.readFrom("genes.toml");
        IMailAPIAdapter apiAdapter = MailtrapAPIAdapter.fromConfig(config);
        apiAdapter.sendReminder("student@test.com", "test message sent");
    }

    @Test
    void setUserRemindedTest() {
        Config config = Config.readFrom("genes.toml");
        String dbFile = config.getDbPath();
        Database.setDbFile(dbFile);
        Database.open();
        LazyList<Reservation> rs = Reservation.find("? > (end - strftime('%s', 'now')) AND NOT is_user_reminded", 300000);
        for(Reservation r : rs) {
            r.setIsUserReminded(true);
            r.saveIt();
        }
        Database.close();
    }
}
