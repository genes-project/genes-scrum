package org.genes.webserver.controller;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.json.JSONObject;
import org.genes.test.IntegrationTestV1;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;


public class LoginControllerTest extends IntegrationTestV1 {

    private static final String JWT_TOKEN_REGEX = "([a-zA-Z0-9\\-_]+\\.){2}[a-zA-Z0-9\\-_]+";

    @ParameterizedTest
    @CsvSource({"student@test.com,STUDENT", "teacher@test.com,TEACHER"})
    public void POST_login_success_returns_a_jwt_token(String username, String expectedRole){
        System.out.println("Trying " + username + " " + expectedRole);
        HttpResponse<JsonNode> response = login(username);

        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(response.getBody()).isNotNull();

        JSONObject body = response.getBody().getObject();
        String role = body.getString("role");
        String jwt = body.getString("jwt");

        assertThat(role).isEqualTo(expectedRole);
        assertThat(jwt).matches(JWT_TOKEN_REGEX);
    }

    @ParameterizedTest
    @ValueSource(strings = {"student@test.com", "teacher@test.com"})
    public void POST_login_wrong_password_returns_403(String username){
        HttpResponse<JsonNode> response = login(username, "wrong");

        assertNoBody(response, 403);
    }

    @Test
    public void POST_login_wrong_username_returns_403(){
        HttpResponse<JsonNode> response = login("wrong@test.com");

        assertNoBody(response, 403);
    }
}
