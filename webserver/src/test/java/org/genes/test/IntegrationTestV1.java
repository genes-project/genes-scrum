package org.genes.test;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;
import org.flywaydb.core.Flyway;
import org.genes.webserver.Database;
import org.genes.webserver.GenesServer;
import org.genes.webserver.Main;
import org.genes.webserver.config.Config;
import org.genes.webserver.model.reservations.Role;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;

import static org.assertj.core.api.Assertions.assertThat;


@Tag("api-v1")
public class IntegrationTestV1 extends IntegrationTest {
    public static final String API_VERSION = "v1";

    private GenesServer server;

    @BeforeEach
    public void beforeEach(){
        loadConfig();
        resetDB();

        server = startServer();

        createUser("student@test.com");
        createUser("teacher@test.com", Role.Type.TEACHER);

        initUnirest(API_VERSION);
    }

    @AfterEach
    public void afterEach(){
        server.stop();
    }

    protected HttpResponse<JsonNode> login(String username){
        return login(username, DEFAULT_PASS, true);
    }

    protected HttpResponse<JsonNode> login(String username, boolean saveToken){
        return login(username, DEFAULT_PASS, saveToken);
    }

    protected HttpResponse<JsonNode> login(String username, String passw){
        return login(username, passw, true);
    }

    protected HttpResponse<JsonNode> login(String username, String passw, boolean saveToken){
        String msg = String.format("{\"email\": \"%s\", \"passwd\": \"%s\"}", username, passw);

        HttpResponse<JsonNode> response = Unirest
                .post("/login")
                .body(msg)
                .asJson();

        if(saveToken && response.getStatus() == 200){
            String jwt = response.getBody().getObject().getString("jwt");

            Unirest.config()
                    .addDefaultHeader("Authorization", "Bearer " + jwt);
        }

        return response;
    }
}
