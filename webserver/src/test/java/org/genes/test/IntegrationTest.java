package org.genes.test;

import kong.unirest.HttpResponse;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;
import org.flywaydb.core.Flyway;
import org.genes.webserver.Database;
import org.genes.webserver.GenesServer;
import org.genes.webserver.Main;
import org.genes.webserver.config.Config;
import org.genes.webserver.model.reservations.Role;
import org.junit.jupiter.api.Tag;

import static org.assertj.core.api.Assertions.assertThat;

@Tag("integration")
public class IntegrationTest {
    public static final String MIGRATION_LOCATION = "src/main/resources/migrations";
    public static final String TEST_CONFIG_LOCATION = "src/test/resources/test.toml";
    public static final String DEFAULT_PASS = "cisco";
    public static final Role.Type DEFAULT_ROLE = Role.Type.STUDENT;

    public static GenesServer startServer(){
        GenesServer server = new GenesServer();
        server.start();
        return server;
    }

    public static void resetDB(){
        Flyway fly = Flyway
                .configure()
                .locations("filesystem:" + MIGRATION_LOCATION)
                .dataSource(
                        Database.getConnectionUrl(),
                        Database.getUserName(),
                        Database.getPassword())
                .load();

        fly.clean();
        fly.migrate();
    }

    public static void loadConfig(){
        loadConfig(TEST_CONFIG_LOCATION);
    }

    public static void loadConfig(String file){
        Config config = Config.readFrom(file);
        Config.setInstance(config);

        Database.setDbFile(config.getDbPath());
    }

    public static void createUser(String username){
        createUser(username, DEFAULT_PASS);
    }

    public static void createUser(String username, String passwd){
        createUser(username, passwd, DEFAULT_ROLE);
    }

    public static void createUser(String username, Role.Type role){
        createUser(username, DEFAULT_PASS, role);
    }

    public static void createUser(String username, String passwd, Role.Type role){
        Database.open();
        Main.getInstance().createUser(username, passwd, role);
        Database.close();
    }

    public static void initUnirest(String apiVersion){
        Unirest.config()
                .reset()
                .cacheResponses(false)
                .automaticRetries(false)
                .connectTimeout(5000)
                .socketTimeout(5000)
                .defaultBaseUrl("http://localhost:80/" + apiVersion);
    }

    protected void assertNoBody(HttpResponse<JsonNode> response, int status){
        assertThat(response.getStatus()).isEqualTo(status);

        assertThat(response.getBody().getObject())
                .satisfies(JSONObject::isEmpty);
    }
}
