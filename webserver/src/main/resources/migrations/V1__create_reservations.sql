-- Table Creation
CREATE TABLE user(
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,

    email VARCHAR(64) UNIQUE,
    pass  BLOB,
    salt BLOB,

    role_id INTEGER,

    CONSTRAINT user_role FOREIGN KEY (role_id) REFERENCES role(id)
);

CREATE TABLE role(
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    name VARCHAR(64)
);

CREATE TABLE reservation(
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,

    start DATETIME,
    end DATETIME,

    user_id INTEGER,
    pc_id INTEGER,

    project_name VARCHAR(256),
    is_user_reminded BOOLEAN DEFAULT FALSE,
    is_topo_deployed BOOLEAN DEFAULT FALSE,

    CONSTRAINT reservation_user FOREIGN KEY (user_id) REFERENCES user(id),
    CONSTRAINT reservation_pc FOREIGN KEY (pc_id) REFERENCES pc(id)
);

CREATE TABLE pc(
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    name VARCHAR(32) UNIQUE,
    address VARCHAR(255) UNIQUE
);

-- Default Roles
INSERT INTO role(name) VALUES ('STUDENT'), ('TEACHER');
