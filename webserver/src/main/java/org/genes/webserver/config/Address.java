package org.genes.webserver.config;

import com.electronwill.nightconfig.core.conversion.Converter;
import lombok.Data;

@Data
public class Address implements Converter<Address, String> {
    private String ip;
    private int port;

    public static Address parse(String address) {
        if(address.isEmpty()) return null;

        Address obj = new Address();
        String[] portAndIp = address.split(":");

        obj.setIp(portAndIp[0]);

        if (portAndIp.length > 1)
            obj.setPort(Integer.parseInt(portAndIp[1]));
        else
            obj.setPort(80);

        return obj;
    }

    @Override
    public Address convertToField(String value) {
        return parse(value);
    }

    @Override
    public String convertFromField(Address value) {
        return String.format("%s:%s", value.getIp(), value.getPort());
    }
}
