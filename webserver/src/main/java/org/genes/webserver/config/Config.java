package org.genes.webserver.config;

import com.electronwill.nightconfig.core.conversion.*;
import com.electronwill.nightconfig.core.file.FileConfig;
import lombok.Getter;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Config {
    private static transient final Logger logger = LoggerFactory.getLogger(Config.class);

    @Getter @Setter
    static Config instance;

    @Getter @Path("app.bind")
    @Conversion(Address.class)
    Address bind;

    @Getter @Path("app.bind-secure")
    @Conversion(Address.class)
    Address bindSecure;

    @Getter @Path("app.http2")
    boolean http2Enabled;

    @Getter @Path("app.log-level")
    LogLevel logLevel;

    @Getter @Path("tls.keystore")
    String keystore;

    @Getter @Path("tls.keystore-pass")
    String keystorePass;

    @Getter @Path("jwt.secret")
    String hmacSecret;

    @Getter @Path("db.path")
    String dbPath;

    @Getter @Path("db.pbkdf2.peanuts")
    String pbkdf2Peanuts;

    @Getter @Path("db.pbkdf2.iterations")
    @SpecIntInRange(min = 128, max = Integer.MAX_VALUE)
    int pbkdf2Iterations;

    @Getter @Path("db.pbkdf2.salt-length")
    @SpecIntInRange(min = 128, max = Integer.MAX_VALUE)
    int pbkdf2SaltLength;

    @Getter @Path("db.pbkdf2.key-length")
    @SpecIntInRange(min = 128, max = Integer.MAX_VALUE)
    int pbkdf2KeyLength;

    @Getter @Path("mail.enabled")
    boolean mailEnabled;

    @Getter @Path("mail.check-interval")
    int mailCheckInterval;

    @Getter @Path("mail.reminder-time")
    int mailReminderTime;

    @Getter @Path("mail.host")
    String mailHost;

    @Getter @Path("mail.username")
    String mailUsername;

    @Getter @Path("mail.password")
    String mailPassword;

    @Getter @Path("mail.from")
    String mailFrom;

    @Getter @Path("deploy.check-interval")
    int deploymentCheckInterval;

    @Getter @Path("deploy.gns3-exe")
    String deploymentGns3Executable;

    @Getter @Path("deploy.username")
    String deploymentUsername;

    @Getter @Path("deploy.password")
    String deploymentPassword;

    @Getter @Path("storage.ip")
    String storageIP;

    @Getter @Path("storage.username")
    String storageUsername;

    @Getter @Path("storage.password")
    String storagePassword;

    public static Config readFrom(String path){
        logger.info("Using config {}", path);

        FileConfig config = FileConfig.of(path);
        config.load();

        ObjectConverter converter = new ObjectConverter();
        return converter.toObject(config, Config::new);
    }
}
