package org.genes.webserver.config;

import com.darkyen.tproll.TPLogger;
import io.javalin.core.JavalinConfig;

public enum LogLevel {
    DEBUG {
        @Override
        public void apply(JavalinConfig config){
            TPLogger.DEBUG();
            config.enableDevLogging();
        }
    },

    INFO {
        @Override
        public void apply(JavalinConfig config) {
            TPLogger.INFO();
        }
    },

    WARN {
        @Override
        public void apply(JavalinConfig config) {
            TPLogger.WARN();
        }
    };

    public abstract void apply(JavalinConfig config);
}
