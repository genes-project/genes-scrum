package org.genes.webserver.controller.reservations;

import io.javalin.http.Context;
import org.genes.webserver.model.reservations.Reservation;
import org.genes.webserver.model.reservations.User;
import org.genes.webserver.security.IAccessManager;

import static org.genes.webserver.model.reservations.Role.Type.*;

public class ReservationAccessManager implements IAccessManager {

    @Override
    public boolean mayCreate(Context ctx, User user) {
        return true;
    }

    @Override
    public boolean mayGetAll(Context ctx, User user) {
        if(user.getRoleType() == TEACHER){
            return true;
        }

        String email = ctx.queryParam("email");
        return user.getRoleType() == STUDENT && email != null && email.equals(user.getEmail());
    }

    @Override
    public boolean mayGetOne(Context ctx, String id, User user) {
        return teacherOrOwn(user, id);
    }

    @Override
    public boolean mayDelete(Context ctx, String id, User user) {
        return teacherOrOwn(user, id);
    }

    @Override
    public boolean mayUpdate(Context ctx, String id, User user) {
        return teacherOrOwn(user, id);
    }

    private boolean teacherOrOwn(User user, String id){
        if(user.getRoleType() == TEACHER){
            return true;
        }

        Reservation reservation = Reservation.findById(id);
        if(reservation == null){
            return true; // Let the reservation controller handle it
        }

        return reservation.parent(User.class).getId().equals(user.getId());
    }
}
