package org.genes.webserver.controller.reservations;

import io.javalin.apibuilder.CrudHandler;
import io.javalin.http.Context;
import org.genes.tools.StorageHandler;
import org.genes.webserver.config.Config;
import org.genes.webserver.model.reservations.PC;
import org.genes.webserver.model.reservations.Reservation;
import org.genes.webserver.model.reservations.User;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class ReservationController implements CrudHandler {
    @Override
    public void create(@NotNull Context ctx) {
        // TODO Pls refactor me when you have more than 3 Brain Cells left @TheHowdy
        Reservation reservation = ctx.bodyAsClass(Reservation.class);

        if (reservation == null) {
            ctx.status(400);
            return;
        }

        if (!projectExists(reservation, Objects.requireNonNull(User.getFromJwt(ctx)))) {
            ctx.status(404);
            return;
        }

        // Means 'any free pc'
        if (reservation.getPCId() == -1) {
            Optional<PC> freePc = PC.findFreePc(reservation.getStart(), reservation.getEnd());

            if (!freePc.isPresent()) {
                ctx.status(400);
                return;
            }

            reservation.setPCId(freePc.get().getId());
        }

        if(!reservation.isPCFree() || reservation.exists()){
            ctx.status(400);
            return;
        }

        reservation.insert();

        User.getFromJwt(ctx).add(reservation);
        ctx.status(200);
    }

    @Override
    public void delete(@NotNull Context ctx, @NotNull String id) {
        Reservation reservation = Reservation.findById(id);

        if (reservation == null)
            ctx.status(404);
        else
            ctx.status(reservation.delete() ? 200 : 400);
    }

    @Override
    public void getAll(Context ctx) {
        String email = ctx.queryParam("email");
        String from = ctx.queryParam("from", "0");
        String to = ctx.queryParam("to", String.valueOf(Long.MAX_VALUE));

        if (email == null) {
            List<Reservation> result = Reservation.find("start between ? and ?", from, to).load();
            ctx.json(result);
            return;
        }

        User user = User.findFirst("email = ?", email);
        if (user == null) {
            ctx.status(404);
            return;
        }

        List<Reservation> result = user.get(Reservation.class, "start between ? and ?", from, to);
        ctx.json(result);
    }

    @Override
    public void getOne(@NotNull Context ctx, @NotNull String id) {
        Reservation reservation = Reservation.findById(id);

        if (reservation == null)
            ctx.status(404);
        else
            ctx.json(reservation);
    }

    @Override
    public void update(@NotNull Context ctx, @NotNull String s) {
        ctx.status(400);
    }

    private boolean projectExists(Reservation reservation, User user) {
        Config config = Config.getInstance();
        StorageHandler storageHandler = new StorageHandler
                ( config.getStorageIP()
                , config.getStorageUsername()
                , config.getStoragePassword());
        return storageHandler.projectExists(reservation.getProjectName(), (int) user.getId());
    }

}