package org.genes.webserver.controller;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.javalin.http.Context;
import io.javalin.http.Handler;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.genes.webserver.model.reservations.User;
import org.genes.webserver.security.Crypto;
import org.jetbrains.annotations.NotNull;

public class LoginController implements Handler {

    @Override
    public void handle(@NotNull Context ctx) {
        LoginMessage loginMessage = ctx.bodyAsClass(LoginMessage.class);

        User user = User.getByEmail(loginMessage.getEmail());
        if(user == null){
            ctx.status(403);
            return;
        }

        boolean valid = Crypto.verify(user, loginMessage.getPasswd());
        if(valid){
            String token = Crypto.getJwtProvider().generateToken(user);
            ctx.json(new Token(token, user.getRoleType().name()));
        }
        else {
            ctx.status(403);
        }
    }

    @Data
    public static class LoginMessage {
        @JsonProperty(required = true)
        private String email;

        @JsonProperty(required = true)
        private String passwd;
    }

    @AllArgsConstructor
    public static class Token {
        @JsonProperty
        private String jwt;

        @JsonProperty
        private String role;
    }
}
