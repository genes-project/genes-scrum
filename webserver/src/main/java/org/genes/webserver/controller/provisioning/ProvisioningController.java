package org.genes.webserver.controller.provisioning;

import io.javalin.http.Context;
import org.genes.tools.Sender;
import org.genes.tools.Starter;
import org.genes.tools.StorageHandler;
import org.genes.webserver.GenesServer;
import org.genes.webserver.config.Config;
import org.genes.webserver.deploy.DeployService;
import org.genes.webserver.model.reservations.User;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;


public class ProvisioningController {
    public static StorageHandler storage = new StorageHandler(
            Config.getInstance().getStorageIP(),
            Config.getInstance().getStorageUsername(),
            Config.getInstance().getStoragePassword());

    public static void upload(Context ctx) {
        try {
            ctx.uploadedFiles().forEach(e -> {
                Path tempFile = Paths.get(GenesServer.CACHE_DIR, e.getFilename());
                try {
                    Files.copy(e.getContent(), tempFile, StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException ioException) {
                    ctx.status(400);
                    ioException.printStackTrace();
                }
                storage.upload(tempFile, (int) User.getFromJwt(ctx).getId());
                tempFile.toFile().delete();
            });
        } catch (Exception ex) {
            ex.printStackTrace();
            ctx.status(400);
        }
    }

    public static void deploy(Context ctx) {
        String projectName;
        String remoteHost;
        Path projectFile = null;
        Config config = Config.getInstance();
        try {
            if ((projectName = ctx.queryParam("projectName")) != null && (remoteHost = ctx.queryParam("remoteHost")) != null) {
                projectFile = Paths.get(GenesServer.CACHE_DIR, projectName);
                storage.download(projectFile, (int) User.getFromJwt(ctx).getId(), projectFile);
                new Sender(
                        remoteHost,
                        DeployService.getRemoteHostDestinationDir(),
                        config.getDeploymentUsername(),
                        config.getDeploymentPassword(),
                        projectFile.toFile()
                ).run();
            } else {
                ctx.status(400);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            ctx.status(400);
        } finally {
            if(projectFile != null) {
                projectFile.toFile().delete();
            }
        }
    }

    public static void start(Context ctx) {
        String projectName;
        String remoteHost;
        Config config = Config.getInstance();
        try {
            if ((projectName = ctx.queryParam("projectName")) != null && (remoteHost = ctx.queryParam("remoteHost")) != null) {
                new Starter(
                        config.getDeploymentUsername(),
                        config.getDeploymentPassword(),
                        remoteHost,
                        config.getDeploymentGns3Executable(),
                        Paths.get(DeployService.getRemoteHostDestinationDir(), projectName)
                ).run();
            } else {
                ctx.status(400);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            ctx.status(400);
        }
    }
}
