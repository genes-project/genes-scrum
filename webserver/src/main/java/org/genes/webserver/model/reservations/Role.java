package org.genes.webserver.model.reservations;

import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.Table;

import java.util.HashMap;
import java.util.Map;

@Table("role")
public class Role extends Model {

    public Type getType(){
        return Type.values()[this.getInteger("id")];
    }

    public enum Type implements io.javalin.core.security.Role {
        NOLOGIN, STUDENT, TEACHER;

        public static Map<String, io.javalin.core.security.Role> getMapping(){
            Map<String, io.javalin.core.security.Role> mapping = new HashMap<>();

            for(int i = 0; i < values().length; i++){
                mapping.put(
                        String.valueOf(i),
                        values()[i]
                );
            }

            return mapping;
        }

        public Role getRole(){
            return Role.findById(this.ordinal());
        }
    }
}
