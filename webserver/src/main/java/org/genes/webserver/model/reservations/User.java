package org.genes.webserver.model.reservations;

import com.auth0.jwt.interfaces.DecodedJWT;
import io.javalin.http.Context;
import javalinjwt.JavalinJWT;
import org.genes.webserver.security.Crypto;
import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.Table;

import java.util.List;

@Table("user")
public class User extends Model {
    public User(){}

    public User(String email, String rawpass){
        set("email", email);

        byte[] salt = Crypto.generateSalt();
        set("salt", salt);

        byte[] hashed = Crypto.hash(rawpass, salt);
        set("pass", hashed);
    }

    public byte[] getPass(){
        return getBytes("pass");
    }

    public byte[] getSalt(){
        return getBytes("salt");
    }

    public String getEmail(){
        return getString("email");
    }

    public int getRoleId(){
        return getInteger("role_id");
    }

    public Role.Type getRoleType(){
        return parent(Role.class).getType();
    }

    public static User getFromJwt(Context ctx){
        if(!JavalinJWT.containsJWT(ctx)){
            return null;
        }

        DecodedJWT jwt = JavalinJWT.getDecodedFromContext(ctx);
        if(jwt == null){
            return null;
        }

        String id = jwt.getClaim("id").asString();

        return User.findById(id);
    }

    public static User getByEmail(String email){
        List<User> users = User.find("email = ?", email);

        if(users.size() != 1){
            // TODO Log something sus
            return null;
        }

        return users.get(0);
    }
}
