package org.genes.webserver.model.reservations;

import org.javalite.activejdbc.LazyList;
import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.Table;

import java.util.Optional;

@Table("pc")
public class PC extends Model {
    public PC(){}

    public PC(String name, String address){
        set("name", name);
        set("address", address);
    }

    public String getName(){
        return getString("name");
    }

    public String getAddress(){
        return getString("address");
    }

    public boolean isFree(long start, long end){
        LazyList<PC> models = PC.findBySQL(
                "SELECT pc.* FROM pc " +
                        "WHERE id NOT IN (" +
                        "SELECT pc.id FROM pc " +
                        "INNER JOIN reservation r ON r.pc_id = pc.id " +
                        "WHERE start <= ? AND end >= ?) AND id = ?" +
                        "LIMIT 1",
                end, start, getLongId());

        return models.size() == 1;
    }

    public static Optional<PC> findFreePc(long start, long end){
        // https://stackoverflow.com/questions/325933/determine-whether-two-date-ranges-overlap
        LazyList<PC> models = PC.findBySQL(
                "SELECT pc.* FROM pc " +
                        "WHERE id NOT IN (" +
                        "SELECT pc.id FROM pc " +
                        "INNER JOIN reservation r ON r.pc_id = pc.id " +
                        "WHERE start <= ? AND end >= ?)" +
                        "LIMIT 1",
                end, start);

        return models.stream().findAny();
    }
}
