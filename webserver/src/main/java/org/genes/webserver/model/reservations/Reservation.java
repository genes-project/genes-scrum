package org.genes.webserver.model.reservations;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.javalite.activejdbc.Model;
import org.javalite.activejdbc.annotations.Table;

import java.time.LocalDateTime;

@Table("reservation")
public class Reservation extends Model {
    public Reservation(){}

    @JsonCreator
    public Reservation(
            @JsonProperty(value = "start", required = true) long start,
            @JsonProperty(value = "end",   required = true) long end,
            @JsonProperty(value = "pc_id", required = true) int pcId,
            @JsonProperty(value = "project_name", required = true) String projectName,
            @JsonProperty(value = "is_user_reminded", required = true) boolean isUserReminded
    ){
        set("start", start);
        set("end", end);
        set("pc_id", pcId);
        set("project_name", projectName);
        set("is_user_reminded", isUserReminded ? 1 : 0);
    }

    public LocalDateTime getStartDateTime(){
        return getTimestamp("start").toLocalDateTime();
    }

    public LocalDateTime getEndDateTime(){
        return getTimestamp("end").toLocalDateTime();
    }

    public void setUser(int id){
        User.findById(id).add(this);
    }

    @JsonIgnore @Override
    public Reservation setId(Object id){
        return super.setId(id);
    }

    @JsonProperty
    public Object getId(){
        return super.getId();
    }

    @JsonProperty
    public long getStart(){
        return getLong("start");
    }

    @JsonProperty
    public long getEnd(){
        return getLong("end");
    }

    @JsonProperty(value = "pc_id")
    public int getPCId(){
        return getInteger("pc_id");
    }

    @JsonProperty
    public String getEmail(){
        // Should never be null
        return this.parent(User.class).getEmail();
    }

    public void setPCId(Object pcId){
        set("pc_id", pcId);
    }

    @JsonProperty(value = "is_user_reminded")
    public boolean getIsUserReminded() { return getBoolean("is_user_reminded"); }

    public void setIsUserReminded(boolean isUserReminded) {
        set("is_user_reminded", isUserReminded);
    }

    public void setTopoDeployed(boolean isTopoDeployed){
        set("is_topo_deployed", isTopoDeployed);
    }

    @JsonProperty(value = "project_name")
    public String getProjectName() {
        return getString("project_name");
    }

    public boolean isPCFree(){
        return parent(PC.class).isFree(getStart(), getEnd());
    }
}
