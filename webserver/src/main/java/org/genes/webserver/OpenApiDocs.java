package org.genes.webserver;

import io.javalin.http.UploadedFile;
import io.javalin.plugin.json.JavalinJackson;
import io.javalin.plugin.openapi.InitialConfigurationCreator;
import io.javalin.plugin.openapi.OpenApiOptions;
import io.javalin.plugin.openapi.OpenApiPlugin;
import io.javalin.plugin.openapi.dsl.OpenApiBuilder;
import io.javalin.plugin.openapi.dsl.OpenApiCrudHandlerDocumentation;
import io.javalin.plugin.openapi.dsl.OpenApiDocumentation;
import io.javalin.plugin.openapi.jackson.JavalinModelResolver;
import io.javalin.plugin.openapi.ui.SwaggerOptions;
import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.genes.webserver.controller.LoginController;
import org.genes.webserver.model.reservations.Reservation;
import org.genes.webserver.model.reservations.Role;

import java.io.File;

import static io.javalin.core.security.SecurityUtil.roles;

public class OpenApiDocs {

    public static OpenApiPlugin setup(){
        Info info = new Info()
                .version(Main.VERSION)
                .description("GENES Web Server");

        InitialConfigurationCreator creator = () ->
                new OpenAPI()
                    .info(info)
                    .addSecurityItem(new SecurityRequirement().addList("bearerAuth"))
                    .components(new Components()
                            .addSecuritySchemes("bearerAuth", getJWTScheme()));

        SwaggerOptions swagger = new SwaggerOptions("/swagger")
                .title("GENES Swagger Docs");

        OpenApiOptions options = new OpenApiOptions(creator)
                .path("/swagger-docs")
                .roles(roles(Role.Type.NOLOGIN))
                .modelConverterFactory(() ->
                        new JavalinModelResolver(JavalinJackson.getObjectMapper()))
                .swagger(swagger);

        return new OpenApiPlugin(options);
    }

    public static OpenApiDocumentation getLoginDocs(){
        return OpenApiBuilder.document()
                .body(LoginController.LoginMessage.class)
                .json("200", LoginController.Token.class);
    }

    public static OpenApiCrudHandlerDocumentation getReservationDocs(){
        return OpenApiBuilder.documentCrud()
                .getAll(OpenApiBuilder.document()
                        .queryParam("email", String.class)
                        .queryParam("from", Integer.class)
                        .queryParam("to", Integer.class)
                        .jsonArray("200", Reservation.class))

                .getOne(OpenApiBuilder.document()
                        .json("200", Reservation.class))

                .create(OpenApiBuilder.document()
                        .body(Reservation.class)
                        .result("200"))

                .update(OpenApiBuilder.document()
                        .result("400"))

                .delete(OpenApiBuilder.document()
                        .result("200"));
    }

    private static SecurityScheme getJWTScheme(){
        return new SecurityScheme()
                .type(SecurityScheme.Type.HTTP)
                .scheme("bearer")
                .bearerFormat("JWT");
    }

    public static OpenApiDocumentation getUploadDocs(){
        return OpenApiBuilder.document()
                .uploadedFile("file")
                .result("200");
    }

    public static OpenApiDocumentation getDeployDocs(){
        return OpenApiBuilder.document()
                .queryParam("projectName", String.class)
                .queryParam("remoteHost", String.class)
                .result("200");
    }

    public static OpenApiDocumentation getStartDocs(){
        return OpenApiBuilder.document()
                .result("200");
    }
}
