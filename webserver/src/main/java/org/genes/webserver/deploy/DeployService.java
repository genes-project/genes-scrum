package org.genes.webserver.deploy;

import org.genes.tools.Sender;
import org.genes.tools.Starter;
import org.genes.tools.StorageHandler;
import org.genes.webserver.Database;
import org.genes.webserver.GenesServer;
import org.genes.webserver.config.Config;
import org.genes.webserver.model.reservations.PC;
import org.genes.webserver.model.reservations.Reservation;
import org.genes.webserver.model.reservations.User;
import org.javalite.activejdbc.LazyList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Timer;
import java.util.TimerTask;

public class DeployService {
    private static Logger logger = LoggerFactory.getLogger(DeployService.class);

    public void start(int checkInterval) {
        new Timer().schedule(new DeployServiceTask(), 0, checkInterval);
    }

    public static String getRemoteHostDestinationDir(){
        String username = Config.getInstance().getDeploymentUsername();

        return String.format("C:\\Users\\%s\\Downloads", username);
    }

    private static class DeployServiceTask extends TimerTask {

        @Override
        public void run() {
            Database.open();

            getAllUndeployedReservations()
                    .stream()
                    .peek(this::setReservationAlreadyDeployed)
                    .forEach(this::deploy);

            Database.close();
        }

        private LazyList<Reservation> getAllUndeployedReservations() {
            return Reservation.find("0 > (start - strftime('%s', 'now')) AND NOT is_topo_deployed");
        }

        private void deploy(Reservation reservation) {
            PC pc = reservation.parent(PC.class);
            User user = reservation.parent(User.class);
            String projectName = reservation.getProjectName();
            Path localProjectPath = Paths.get(GenesServer.CACHE_DIR, projectName);

            logger.info("Deploying {} for {} to {} ({}) ...",
                    projectName, user.getEmail(), pc.getName(), pc.getAddress());

            try {
                // Download
                Path shareProjectPath = Paths.get(projectName);
                storage().download(shareProjectPath, (int)user.getId(), localProjectPath);

                // Send to remote
                Path remoteProjectPath = Paths.get(getRemoteHostDestinationDir(), projectName);
                Sender sender = sender(pc, remoteProjectPath, localProjectPath);

                sender.run();

                // Start on remote
                Starter starter = starter(pc, remoteProjectPath);
                starter.run();

                logger.info("Successfully deployed to {}", pc.getName());
            }
            finally {
                // Just ignore this
                try {
                    Files.deleteIfExists(localProjectPath);
                } catch (IOException e) {
                    logger.error("Could not delete locally cached project", e);
                }
            }
        }

        private void setReservationAlreadyDeployed(Reservation reservation) {
            reservation.setTopoDeployed(true);
            reservation.saveIt();
        }

        private Starter starter(PC pc, Path remoteProjectPath){
            Config config = Config.getInstance();

            return new Starter(
                    config.getDeploymentUsername(),
                    config.getDeploymentPassword(),
                    pc.getAddress(),
                    config.getDeploymentGns3Executable(),
                    remoteProjectPath
            );
        }

        private Sender sender(PC pc, Path remoteProjectPath, Path localProjectPath){
            Config config = Config.getInstance();

            return new Sender(
                    pc.getAddress(),
                    remoteProjectPath.toString(),
                    config.getDeploymentUsername(),
                    config.getDeploymentPassword(),
                    localProjectPath.toFile());
        }

        private StorageHandler storage(){
            Config config = Config.getInstance();

            return new StorageHandler(
                    config.getStorageIP(),
                    config.getStorageUsername(),
                    config.getStoragePassword());
        }
    }
}
