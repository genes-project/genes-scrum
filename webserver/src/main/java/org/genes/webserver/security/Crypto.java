package org.genes.webserver.security;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import javalinjwt.JWTGenerator;
import javalinjwt.JWTProvider;
import lombok.Getter;
import org.genes.webserver.config.Config;
import org.genes.webserver.model.reservations.User;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;

public class Crypto {
    @Getter
    private static JWTProvider jwtProvider;

    static {
        // https://javalin.io/2018/09/11/javalin-jwt-example.html
        String secret = Config.getInstance().getHmacSecret();

        Algorithm algorithm = Algorithm.HMAC256(secret);

        JWTGenerator<User> generator = (user, alg) -> {
            JWTCreator.Builder token = JWT.create()
                    .withClaim("id", user.getId().toString())
                    .withClaim("role", String.valueOf(user.getRoleId()));

            return token.sign(alg);
        };

        JWTVerifier verifier = JWT.require(algorithm).build();
        Crypto.jwtProvider = new JWTProvider(algorithm, generator, verifier);
    }

    /**
     * Hashing with a salt and peanuts/pepper, see https://crypto.stackexchange.com/a/8290.
     */
    public static byte[] hash(String passw, byte[] salt) {
        // Get stuff from config
        Config config = Config.getInstance();
        int iterations = config.getPbkdf2Iterations();
        int keyLen = config.getPbkdf2KeyLength();
        String peanuts = config.getPbkdf2Peanuts();

        byte[] hmac = hmac512(
                passw.getBytes(StandardCharsets.UTF_8),
                peanuts.getBytes(StandardCharsets.UTF_8)
        );

        char[] data = new String(hmac, StandardCharsets.UTF_8).toCharArray();

        return pbkdf2(
            data,
            salt,
            iterations,
            keyLen
        );
    }

    public static boolean verify(User user, String passw){
        return verify(user.getPass(), user.getSalt(), passw);
    }

    public static boolean verify(byte[] hashed, byte[] salt, String passw){
        return Arrays.equals(hashed, hash(passw, salt));
    }

    public static byte[] generateSalt(){
        int len = Config.getInstance().getPbkdf2SaltLength();
        return generateSalt(len);
    }

    public static byte[] generateSalt(int len){
        SecureRandom random = new SecureRandom();

        byte[] buffer = new byte[len];
        random.nextBytes(buffer);

        return buffer;
    }

    private static byte[] pbkdf2(char[] data, byte[] salt, int iterations, int keyLen){
        try {
            SecretKeyFactory secretKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512");

            PBEKeySpec dynamicHash = new PBEKeySpec(
                    data,
                    salt,
                    iterations,
                    keyLen);

            SecretKey dynKey = secretKeyFactory.generateSecret(dynamicHash);

            return dynKey.getEncoded();
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            e.printStackTrace(); // TODO Handle
            throw new RuntimeException("Could not hash passwd!", e);
        }
    }

    private static byte[] hmac512(byte[] data, byte[] secret){
        try {
            // https://stackoverflow.com/questions/39355241/compute-hmac-sha512-with-secret-key-in-java
            SecretKeySpec secretKeySpec = new SecretKeySpec(secret, "HmacSHA512");

            Mac mac = Mac.getInstance("HmacSHA512");
            mac.init(secretKeySpec);

            return mac.doFinal(data);
        }
        catch (NoSuchAlgorithmException | InvalidKeyException e){
            e.printStackTrace();
            throw new RuntimeException("Could not hmac data!", e);
        }
    }
}
