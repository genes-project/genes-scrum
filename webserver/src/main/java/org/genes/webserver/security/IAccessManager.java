package org.genes.webserver.security;

import com.auth0.jwt.interfaces.DecodedJWT;
import io.javalin.apibuilder.CrudHandler;
import io.javalin.http.Context;
import javalinjwt.JavalinJWT;
import org.genes.webserver.model.reservations.User;
import org.jetbrains.annotations.NotNull;

public interface IAccessManager {

    boolean mayCreate(Context ctx,User user);

    boolean mayGetAll(Context ctx, User user);

    boolean mayGetOne(Context ctx, String arg, User user);

    boolean mayDelete(Context ctx, String arg, User user);

    boolean mayUpdate(Context ctx, String arg, User user);

    static CrudHandler checked(CrudHandler handler, IAccessManager accessManager){
        return new AccessWrapper(handler, accessManager);
    }

    class AccessWrapper implements CrudHandler {

        private CrudHandler handler;
        private IAccessManager manager;

        public AccessWrapper(CrudHandler handler, IAccessManager manager){
            this.handler = handler;
            this.manager = manager;
        }

        @Override
        public void create(@NotNull Context ctx) {
            User user = getUserFromJwt(ctx);

            if(user != null && manager.mayCreate(ctx, user))
                handler.create(ctx);
            else
                ctx.status(401);

        }

        @Override
        public void delete(@NotNull Context ctx, @NotNull String s) {
            User user = getUserFromJwt(ctx);

            if(user != null && manager.mayDelete(ctx, s, user))
                handler.delete(ctx, s);
            else
                ctx.status(401);
        }

        @Override
        public void getAll(@NotNull Context ctx) {
            User user = getUserFromJwt(ctx);

            if(user != null && manager.mayGetAll(ctx, user))
                handler.getAll(ctx);
            else
                ctx.status(401);

        }

        @Override
        public void getOne(@NotNull Context ctx, @NotNull String s) {
            User user = getUserFromJwt(ctx);

            if(user != null && manager.mayGetOne(ctx, s, user))
                handler.getOne(ctx, s);
            else
                ctx.status(401);
        }

        @Override
        public void update(@NotNull Context ctx, @NotNull String s) {
            User user = getUserFromJwt(ctx);

            if(user != null && manager.mayUpdate(ctx, s, user))
                handler.update(ctx, s);
            else
                ctx.status(401);
        }

        private User getUserFromJwt(Context ctx){
            DecodedJWT jwt = JavalinJWT.getDecodedFromContext(ctx);
            User user = User.getFromJwt(ctx);

            String role = String.valueOf(user.getRoleId());
            String claimedRole = jwt.getClaim("role").asString();

            if(!role.equals(claimedRole))
                user = null;

            return user;
        }
    }
}
