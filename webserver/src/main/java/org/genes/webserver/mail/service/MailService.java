package org.genes.webserver.mail.service;

import lombok.AllArgsConstructor;
import org.genes.webserver.Database;
import org.genes.webserver.mail.apiadapter.IMailAPIAdapter;
import org.genes.webserver.model.reservations.Reservation;
import org.genes.webserver.model.reservations.User;
import org.javalite.activejdbc.LazyList;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Timer;
import java.util.TimerTask;

@AllArgsConstructor
public class MailService {

    private IMailAPIAdapter apiAdapter;

    public void start(int checkInterval, int reminderTime) {
        new Timer().schedule(new MailServiceTask(reminderTime, apiAdapter), 0, checkInterval);
    }

}

@AllArgsConstructor
class MailServiceTask extends TimerTask {

    private static Logger logger = LoggerFactory.getLogger(MailServiceTask.class);

    private final int reminderTime;
    private final IMailAPIAdapter apiAdapter;

    @Override
    public void run() {
        Database.open();

        getAllReservationsToRemind().stream()
                .peek(this::setReservationAlreadyReminded)
                .map(this::getAssociativeUser)
                .forEach(this::sendReminder);

        Database.close();
    }

    private void setReservationAlreadyReminded(Reservation reservation) {
        reservation.setIsUserReminded(true);
        reservation.saveIt();
    }

    private LazyList<Reservation> getAllReservationsToRemind() {
        return Reservation.find("? > (start - strftime('%s', 'now')) AND NOT is_user_reminded", reminderTime);
    }

    private User getAssociativeUser(Reservation reservation) {
        return reservation.parent(User.class);
    }

    private void sendReminder(User toUser) {
        logger.info("Sending reminder to {}", toUser.getEmail());

        apiAdapter.sendReminder(toUser.getEmail(), "Ihre Reservierungszeit wird in Kürze eintreffen!");
    }

}
