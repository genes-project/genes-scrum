package org.genes.webserver.mail.apiadapter;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.genes.webserver.config.Config;
import org.jetbrains.annotations.NotNull;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

@AllArgsConstructor
public class MailtrapAPIAdapter implements IMailAPIAdapter {

    private String host, from, username, password;

    public static MailtrapAPIAdapter fromConfig(Config config) {
        return new MailtrapAPIAdapter
                ( config.getMailHost()
                , config.getMailFrom()
                , config.getMailUsername()
                , config.getMailPassword());
    }

    @SneakyThrows
    @Override
    public void sendReminder(String receiver, String message) {
        Session session = buildSession(host, username, password);
        Transport.send(buildMimeMessage(receiver, message, from, session));
    }

    @NotNull
    private Properties propertiesFromHost(String host) {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", host);
        props.put("mail.smtp.port", "2525");
        return props;
    }

    @NotNull
    private Message buildMimeMessage(String receiver, String message, String from, Session session) throws MessagingException {
        Message mimeMessage = new MimeMessage(session);
        mimeMessage.setFrom(new InternetAddress(from));
        mimeMessage.setRecipients(Message.RecipientType.TO,
                InternetAddress.parse(receiver));
        mimeMessage.setSubject("GENES | Reservation reminder");
        mimeMessage.setText(message);
        return mimeMessage;
    }

    @NotNull
    private Session buildSession(String host, String username, String password) {
        return Session.getInstance(propertiesFromHost(host),
                new Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });
    }

}
