package org.genes.webserver.mail.apiadapter;

public interface IMailAPIAdapter {

    void sendReminder(String receiver, String message);

}
