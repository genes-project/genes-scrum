package org.genes.webserver;

import com.darkyen.tproll.TPLogger;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.javalin.Javalin;
import io.javalin.apibuilder.CrudHandler;
import io.javalin.core.JavalinConfig;
import io.javalin.http.Context;
import io.javalin.http.Handler;
import io.javalin.http.staticfiles.Location;
import io.javalin.plugin.json.JavalinJackson;
import io.javalin.plugin.openapi.dsl.OpenApiCrudHandlerDocumentation;
import io.javalin.plugin.openapi.dsl.OpenApiDocumentation;
import javalinjwt.JWTAccessManager;
import javalinjwt.JavalinJWT;
import org.eclipse.jetty.alpn.server.ALPNServerConnectionFactory;
import org.eclipse.jetty.http2.HTTP2Cipher;
import org.eclipse.jetty.http2.server.HTTP2ServerConnectionFactory;
import org.eclipse.jetty.server.*;
import org.eclipse.jetty.util.ssl.SslContextFactory;
import org.genes.webserver.config.Config;
import org.genes.webserver.config.LogLevel;
import org.genes.webserver.controller.LoginController;
import org.genes.webserver.controller.provisioning.ProvisioningController;
import org.genes.webserver.controller.reservations.ReservationAccessManager;
import org.genes.webserver.controller.reservations.ReservationController;
import org.genes.webserver.model.reservations.Role;
import org.genes.webserver.security.Crypto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static io.javalin.apibuilder.ApiBuilder.*;
import static io.javalin.core.security.SecurityUtil.roles;
import static io.javalin.plugin.openapi.dsl.OpenApiBuilder.documented;
import static org.genes.webserver.security.IAccessManager.checked;

public class GenesServer {
    private static final Logger logger = LoggerFactory.getLogger(GenesServer.class);

    public static final String WWW_DIR = "public";

    public static final String CACHE_DIR = "cache";

    private Javalin app;

    public void start(){
        createCacheDirectory();

        app = Javalin
                .create(this::configureServer)
                .routes(this::setupEndpoints)
                .start();
    }

    public void stop(){
        app.stop();
    }

    private void createCacheDirectory(){
        try {
            Path cache = Paths.get(CACHE_DIR);

            if(!Files.isDirectory(cache)) {
                Files.createDirectory(cache);
            }
        }
        catch (IOException e) {
            logger.error("Could not create cache dir", e);
        }
    }

    private void configureServer(JavalinConfig config){
        config
                .server(this::createServer)
                .addStaticFiles("/login", "public/html/login.html", Location.EXTERNAL)
                .addStaticFiles("/", "public/html/index.html", Location.EXTERNAL)
                .addStaticFiles("/prov", "public/html/provisionierung.html", Location.EXTERNAL)
                .addStaticFiles("/", WWW_DIR, Location.EXTERNAL);

        setupJackson();

        setupLogging(config);

        setupOpenApiDocs(config);

        setupAccessManager(config);
    }

    private Server createServer(){
        Config config = Config.getInstance();

        // https://github.com/tipsy/javalin-http2-example
        Server server = new Server();

        if(config.getBind() != null){
            int port = config.getBind().getPort();
            ServerConnector connector = new ServerConnector(server);
            connector.setPort(port);
            server.addConnector(connector);
        }

        if(config.getBindSecure() != null) {
            int sslPort = config.getBindSecure().getPort();
            // HTTP Configuration
            HttpConfiguration httpConfig = new HttpConfiguration();
            httpConfig.setSendServerVersion(false);
            httpConfig.setSecureScheme("https");
            httpConfig.setSecurePort(sslPort);

            // SSL Context Factory for HTTPS and HTTP/2
            SslContextFactory.Server sslContextFactory = new SslContextFactory.Server.Server();


            sslContextFactory.setKeyStorePath(config.getKeystore());
            sslContextFactory.setKeyStorePassword(config.getKeystorePass());
            sslContextFactory.setCipherComparator(HTTP2Cipher.COMPARATOR);
            sslContextFactory.addExcludeProtocols("SSLv3", "TLSv1", "TLSv1.1");
            sslContextFactory.setProvider("Conscrypt");

            // HTTPS Configuration
            HttpConfiguration httpsConfig = new HttpConfiguration(httpConfig);
            httpsConfig.addCustomizer(new SecureRequestCustomizer());

            // HTTP/2 Connector
            if(config.isHttp2Enabled()){
                // HTTP/2 Connection Factory
                HTTP2ServerConnectionFactory h2 = new HTTP2ServerConnectionFactory(httpsConfig);
                ALPNServerConnectionFactory alpn = new ALPNServerConnectionFactory();
                alpn.setDefaultProtocol("h2");

                // SSL Connection Factory
                SslConnectionFactory ssl = new SslConnectionFactory(sslContextFactory, alpn.getProtocol());

                ServerConnector http2Connector = new ServerConnector(server, ssl, alpn, h2, new HttpConnectionFactory(httpsConfig));
                http2Connector.setPort(sslPort);
                server.addConnector(http2Connector);
            }
            else {
                SslConnectionFactory ssl = new SslConnectionFactory(sslContextFactory, "http/1.1");
                ServerConnector connector = new ServerConnector(server, ssl, new HttpConnectionFactory(httpConfig));
                connector.setPort(sslPort);
                server.addConnector(connector);
            }
        }

        if(server.getConnectors().length == 0){
            // TODO Log an error
            System.exit(-1);
        }

        return server;
    }

    private void setupEndpoints(){
        before(this::setupJwtHandler);

        path("/v1", () -> {
            before(ctx -> Database.open());
            after(ctx -> Database.close());

            crud("/reservations/:res-id",
                    getReservationHandler(),
                    roles(Role.Type.STUDENT, Role.Type.TEACHER));

            post("/login",
                    getLoginHandler(),
                    roles(Role.Type.NOLOGIN));

            path("/prov", () -> {
                post("/upload",
                        getUploadHandler(),
                        roles(Role.Type.STUDENT, Role.Type.TEACHER));

                get("/deploy",
                        getDeployHandler(),
                        roles(Role.Type.TEACHER));

                get("/start",
                        getStartHandler(),
                        roles(Role.Type.TEACHER));
            });
        });
    }

    private CrudHandler getReservationHandler(){
        OpenApiCrudHandlerDocumentation docs = OpenApiDocs.getReservationDocs();

        CrudHandler checked = checked(new ReservationController(), new ReservationAccessManager());

        return documented(docs, checked);
    }

    private Handler getLoginHandler(){
        OpenApiDocumentation docs = OpenApiDocs.getLoginDocs();

        return documented(docs, new LoginController());
    }

    private Handler getUploadHandler(){
        OpenApiDocumentation docs = OpenApiDocs.getUploadDocs();

        return documented(docs, ProvisioningController::upload);
    }

    private Handler getDeployHandler(){
        OpenApiDocumentation docs = OpenApiDocs.getDeployDocs();

        return documented(docs, ProvisioningController::deploy);
    }

    private Handler getStartHandler(){
        OpenApiDocumentation docs = OpenApiDocs.getStartDocs();

        return documented(docs, ProvisioningController::start);
    }

    private void setupJwtHandler(Context ctx) throws Exception {
        JavalinJWT
                .createHeaderDecodeHandler(Crypto.getJwtProvider())
                .handle(ctx);
    }

    private void setupLogging(JavalinConfig config){
        Config.getInstance()
                .getLogLevel()
                .apply(config);

        TPLogger.attachUnhandledExceptionLogger();

        config.requestLogger((ctx, ms) ->
            logger.info("{} {} {} took {}ms", ctx.ip(), ctx.url(), ctx.status(), ms)
        );
    }

    private void setupOpenApiDocs(JavalinConfig config){
        config.registerPlugin(OpenApiDocs.setup());
    }

    private void setupAccessManager(JavalinConfig config){
        JWTAccessManager accessManager = new JWTAccessManager(
                "role",
                Role.Type.getMapping(),
                Role.Type.NOLOGIN);

        config.accessManager(accessManager);
    }

    private void setupJackson(){
        // We have to restrict our JSON mapper to serialize only explicitly marked field
        // to be able to correctly marshal ActiveJDBC Models
        ObjectMapper mapper = new ObjectMapper();
        mapper.setVisibility(mapper.getSerializationConfig().getDefaultVisibilityChecker()
                .withFieldVisibility(Visibility.NONE)
                .withGetterVisibility(Visibility.NONE)
                .withSetterVisibility(Visibility.NONE)
                .withCreatorVisibility(Visibility.NONE)
                .withIsGetterVisibility(Visibility.NONE)
                .withCreatorVisibility(Visibility.NONE));

        JavalinJackson.configure(mapper);
    }
}
