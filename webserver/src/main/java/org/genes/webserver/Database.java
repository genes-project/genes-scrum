package org.genes.webserver;

import lombok.Getter;
import lombok.Setter;
import org.javalite.activejdbc.Base;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Database {
    private static Logger logger = LoggerFactory.getLogger(Database.class);

    @Getter @Setter
    private static String dbFile;

    @Getter
    private static String userName = "root";

    @Getter
    private static String password = "";

    public static String getConnectionUrl(){
        return "jdbc:sqlite:" + getDbFile();
    }

    public static void open(){
        try {
            Base.open("org.sqlite.JDBC", getConnectionUrl(), getUserName(), getPassword());
        }
        catch (Exception e){
            logger.warn("Could not open DB", e);
        }
    }

    public static void close(){
        try {
            Base.close();
        }
        catch (Exception e){
            logger.warn("Could not open DB", e);
        }
    }
}
