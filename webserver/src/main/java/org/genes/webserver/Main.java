package org.genes.webserver;

import lombok.Getter;
import org.genes.webserver.config.Config;
import org.genes.webserver.config.LogLevel;
import org.genes.webserver.deploy.DeployService;
import org.genes.webserver.mail.service.MailService;
import org.genes.webserver.mail.apiadapter.IMailAPIAdapter;
import org.genes.webserver.mail.apiadapter.MailtrapAPIAdapter;
import org.genes.webserver.model.reservations.PC;
import org.genes.webserver.model.reservations.Role;
import org.genes.webserver.model.reservations.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import picocli.CommandLine;
import static picocli.CommandLine.*;

@Command(name = "genes-webserver", mixinStandardHelpOptions = true, version = "v0.0.1",
        description = "The GENES Webserver.")
public class Main implements Runnable {

    public static final String VERSION = "0.0.1";

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    @Getter
    private static final Main instance = new Main();

    @Option(names = {"-c", "--config"}, defaultValue = "genes.toml", description = "The config file to use")
    private Config configFile;

    @Override
    public void run() {
        Config.setInstance(configFile);

        String dbFile = configFile.getDbPath();
        Database.setDbFile(dbFile);

        if(configFile.isMailEnabled()) {
            IMailAPIAdapter apiAdapter = MailtrapAPIAdapter.fromConfig(configFile);
            new MailService(apiAdapter)
                    .start(configFile.getMailCheckInterval(), configFile.getMailReminderTime());
            new MailService(apiAdapter).start(configFile.getMailCheckInterval(), configFile.getMailReminderTime());
        }

        new DeployService().start(
                configFile.getDeploymentCheckInterval());

        new GenesServer().start();
    }

    @Command(name = "newuser", description = "Creates a new User in the DB and exits.")
    public void createUserCommand(
            @Parameters(index = "0", description = "username")
                    String username,
            @Parameters(index = "1", description = "password")
                    String password,
            @Option(names = {"-r", "--role"}, defaultValue = "STUDENT", description = "role, aka. STUDENT or TEACHER")
                    Role.Type roleType
    ){
        Config.setInstance(configFile);

        String dbFile = Config.getInstance().getDbPath();
        Database.setDbFile(dbFile);
        Database.open();

        createUser(username, password, roleType);

        Database.close();
        System.exit(0);
    }

    @Command(name = "newpc", description = "Creates a new PC in the DB and exits.")
    public void createPCCommand(
            @Parameters(index = "0", description = "name")
                    String name,
            @Parameters(index = "1", description = "address")
                    String address
    ){
        Config.setInstance(configFile);

        String dbFile = Config.getInstance().getDbPath();
        Database.setDbFile(dbFile);
        Database.open();

        createPC(name, address);

        Database.close();
        System.exit(0);
    }

    public void createUser(String username, String password, Role.Type roleType){
        logger.info("Creating user {}", username);

        User user = new User(username, password);
        user.saveIt();

        roleType.getRole().add(user);
    }

    public void createPC(String name, String address){
        logger.info("Creating pc {}", name);

        PC pc = new PC(name, address);
        pc.saveIt();
    }

    public static void main(String[] args) {
        new CommandLine(getInstance())
                .registerConverter(Config.class, Config::readFrom)
                .registerConverter(Role.Type.class, Role.Type::valueOf)
                .registerConverter(LogLevel.class, LogLevel::valueOf)
                .execute(args);
    }
}
