# API Docs (v1)
> Deprecated: Use swagger docs instead!

## `/login`

- Example Request:

```json
# curl -X POST localhost/v1/login -d
{
    "email": "user@example.com",
    "passwd": "xdfcgvbhjnbgfvddasdsa"
}
```

- Example Response:

```json
{
    "jwt": "a json web token: https://jwt.io"
}
```

## `/reservations/:id`

> **Note:**
> All requests expect a JWT token provided in the header

### Model

```json
{
    "id": 3, # Field is ignored on POST
    "start": 345672312, # Unix Time
    "end": 34567890, # Unix Time
    "pc_id": -1 # PC ID, -1 for any
}
```

### Get all

- Example Request:

```bash
curl -H "Authorization: Bearer <JWT TOKEN>" localhost/v1/reservations # Prints multiple models
```

### Post

```bash
curl -H "Authorization: Bearer <JWT TOKEN>"  -X POST localhost/reservations -d <See Model>
```

### Delete

```bash
curl -H "Authorization: Bearer <JWT TOKEN>"  -X DELETE localhost/v1/reservations/1
```

### Get all reservations for day

```bash
curl -H "Authorization: Bearer <JWT TOKEN>"  localhost/v1/reservations?from=23123123&to=3213312312 # Unix Time, Prints multiple models
```

#### Get all reservations for user

```bash
curl -H "Authorization: Bearer <JWT TOKEN>"  localhost/v1/reservations?email=test.com
```

