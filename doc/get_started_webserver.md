# Webserver

## Introduction

The webserver is the core part of the application and serves as an interface for the user to make use of the genes-tools.

## Setup / Prerequisites 

Before starting the webserver, ensure that the following points have been covered:

### Operating Systems

The application only supports Windows as the underlying OS. 

### Proper directory structure

Since the java-application relies on some dependencies as well as configuration files, it will try to search for them in specified locations. When using the provided batch-file ensure that the files are contained in the following manner

```
genes-0.0.1
│   genes-tools.bat
│   genes-webserver.bat  # Execute this file to start the webserver
│   genes.toml 			# The configuration file 
├───bin
│   │   genes-tools.jar
│   │   genes-webserver.jar
│   └───deps
│           [...]
├───data
│   │   genes.sqlite	# The database
│   └───migrations
│           [...]
└───public
    ├───css
    │       [...]
    ├───html
    │       [...]
    │
    ├───icons
    │       [...]
    │
    └───script
            [...]
```

### PsExec installation

In order to start a GUI-Program remotely the server makes use of the [PsExec](https://docs.microsoft.com/en-us/sysinternals/downloads/psexec) program. Therefore the server will need to have this program installed and added to the System-Path.

### Client-Workstations as trusted hosts

The webserver needs to have the client-workstation PCs in its trusted-hosts list for in order to be able to send out the WinRM- and PsExec-Commands. This can be done using the following Powershell-command as an Administrator:

```powershell
Set-Item wsman:\localhost\Client\TrustedHosts -value <PC1>[,<PC2>, ...]
```

To check if everything has been set correctly, use the following command:

```powershell
Get-Item wsman:\localhost\Client\TrustedHosts
```

## Configuration

Finally, configure the webserver using the `genes.toml` file. An example configuration can be seen below.

```toml
# GENES Webserver Config File

[app]
# Set to an empty string to disable non-HTTPS
bind = "0.0.0.0:80"
# To enable TLS, set 'bind-secure' to an address with port and configure the [tls] section
bind-secure = ""
# HTTP/2 requires TLS
http2 = false
# Possible values: DEBUG, INFO, WARN
log-level = "INFO"

[tls]
# A local certificate can be generated with
# keytool -genkey -alias mydomain -keyalg RSA -keystore keystore.jks -keysize 2048
keystore = "keystore.jks"
keystore-pass = "CHANGEME"

[jwt]
# Please change this
secret = "CHANGEME"

[db]
path = "data/genes.sqlite"

[db.pbkdf2]
# Also called pepper, set this to something secure
peanuts = "CHANGEME"
# PBKDF2 parameters, can be left as-is
iterations = 10000
salt-length = 128
key-length = 512

[mail]
# Disable the webserver
enabled = true
# Interval for checking if new mails should be send in ms (1s per default)
check-interval = 10000
# Time before reservation when a user is notified, in ms (5min per default)
reminder-time = 300_000
# SMTP host / creds / from
host=""
username=""
password=""
from=""

[deploy]
# Interval for checking if a topology is to be deployed
check-interval = 10000
# Path to the remote GNS3 executable on the Workstation
gns3-exe = "C:\\Program Files\\GNS3\\gns3.exe"
# Workstation creds
username = "junioradmin"
password = "junioradmin"

[storage]
# FTP storage address and username
ip="CHANGEME"
username="CHANGEME"
password="CHANGEME"
```

