# Tools
## Introduction
The tools section of the genes project consists of several commands, which are explained in this document.
The tools part of genes can be access by any command line tool like PowerShell or CMD.
Additionally, the application can also be called with `genes-tools.bat` located in the _tools_ folder.
For help texts within the application, use the `--help` option.

## Commands
| Command    | Description                                                                    |
| ---------: | ------------------------------------------------------------------------------ |
| `download` | Downloads a genes-project from the GENES-FTP server to the specified location. |
| `export`   | Exports a GNS3 Project to a given file.                                        |
| `run`      | Combines export, send and start.                                               |
| `send`     | Sends a exported GNS3 Project to a given PC.                                   |
| `start`    | Starts a GNS3 Project on a given PC.                                           |
| `upload`   | Uploads a genes-project to the GENES-FTP server.                               |

For further help texts to command parameters and options, use the desired command plus `--help`.