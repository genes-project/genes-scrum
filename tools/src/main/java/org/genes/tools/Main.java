package org.genes.tools;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Parameters;
import picocli.CommandLine.Option;

import javax.swing.*;
import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

@Command(name = "genes", mixinStandardHelpOptions = true, version = "GENES 0.1",
        description = "GENES: Exports, sends and starts a project in GNS3 to another computer.")
public class Main {

    public static void main(String[] args) {
        new CommandLine(new Main()).execute(args);
    }

    @Command(name = "run", description = "Combines export, send and start.")
    public void run(
        @Parameters(index = "0", description = "Server IP and optional Port (e.g. \"192.168.0.31\" or \"localhost:80\")")
                String server,
        @Parameters(index = "1", description = "Project Name, case sensitive! (e.g. \"project_OSPF\")")
                String projectName,
        @Parameters(index = "2", description = "Remote host (e.g. \"Server1\" or \"192.168.0.30\"")
                String remoteHost,
        @Parameters(index = "3", description = "Remote host destination (e.g. \"C:\\\"")
                String remoteHostDestination,
        @Parameters(index = "4", description = "Remote username (e.g. \"Junioradmin\")")
                String remoteUser,
        @Parameters(index = "5", description = "Remote password (e.g. \"password123\")")
                String remotePassword,
        @Option(names = {"-f", "--file"}, description = "Save path (e.g. \"C:\\Users\\user\\Downloads\\export.gns3project\")")
                Path fileName
    ) {
        File fileToExport = export(server, projectName, fileName);
        System.out.println();
        send(remoteHost, remoteHostDestination, remoteUser, remotePassword, fileToExport.toPath());
        Path projectPath =
                fileName == null ? Paths.get(projectName + ".gns3project") : fileName.toAbsolutePath();
        start(remoteHost, projectPath);
    }

    @Command(name = "export", description = "Exports a GNS3 Project to a given file.")
    public File export(
        @Parameters(index = "0", description = "Server IP and optional Port (e.g. \"192.168.0.31\" or \"localhost:80\")")
                String server,
        @Parameters(index = "1", description = "Project Name, case sensitive! (e.g. \"project_OSPF\")")
                String projectName,
        @Option(names = {"-f", "--file"}, description = "Save path (e.g. \"C:\\Users\\user\\Downloads\\export.gns3project\")")
                Path fileName
    ) {
        File fileToExport;
        if(fileName == null) {
            fileToExport = new File(projectName + ".gns3project");
        } else {
            fileToExport = fileName.toFile();
        }
        GNSExporter exporter = new GNSExporter(server, projectName, fileToExport);
        return exporter.call();
    }

    @Command(name = "send", description = "Sends a exported GNS3 Project to a given PC.")
    public void send(
            @Parameters(index = "0", description = "Remote host (e.g. \"Server1\" or \"192.168.0.30\")")
                    String remoteHost,
            @Parameters(index = "1", description = "Remote host destination (e.g. \"C:\\\")")
                    String remoteHostDestination,
            @Parameters(index = "2", description = "Remote username (e.g. \"Junioradmin\")")
                    String remoteUser,
            @Parameters(index = "3", description = "Remote password (e.g. \"password123\")")
                    String remotePassword,
            @Parameters(index = "4", description = "Sending file (e.g. \"C:\\Users\\user\\Downloads\\export.gns3project\")")
                    Path fileName

    ) {
        Sender sender = new Sender(remoteHost, remoteHostDestination, remoteUser, remotePassword, fileName.toFile());
        sender.run();
    }

    @Command(name = "start", description = "Starts a GNS3 Project on a given PC.")
    public void start(
            @Parameters(index = "0", description = "Remote host (e.g. \"Server1\" or \"192.168.0.30\"")
                String remoteHost,
            @Parameters(index = "1", description = "Path of the GNS3 Project file to open")
                Path projectPath
    ) {
        new Starter("junioradmin", "junioradmin", remoteHost, "C:\\Program Files\\GNS3\\gns3.exe", projectPath).run();
    }

    @Command(name = "upload", description = "Uploads a genes-project to the GENES-FTP server.")
    public void upload(
            @Parameters(index = "0", description = "The IP of the FTP-Server")
                String storageIP,
            @Parameters(index = "1", description = "The username to login as")
                String username,
            @Parameters(index = "2", description = "The password to use")
                String password,
            @Parameters(index = "3", description = "The project to upload")
                Path projectToUpload,
            @Parameters(index = "4", description = "The User-ID indicating to whom this project belongs to")
                int userID
    ) {
        new StorageHandler(storageIP, username, password)
                .upload(projectToUpload, userID);
    }

    @Command(name = "download", description = "Downloads a genes-project from the GENES-FTP server to the specified location.")
    public void download(
            @Parameters(index = "0", description = "The IP of the FTP-Server")
                String storageIP,
            @Parameters(index = "1", description = "The username to login as")
                String username,
            @Parameters(index = "2", description = "The password to use")
                String password,
            @Parameters(index = "3", description = "The project to download")
                Path projectToDownload,
            @Parameters(index = "4", description = "The User-ID indicating to whom this project belongs to")
                int userID,
            @Parameters(index = "5", description = "The location where to save the project")
                Path downloadPath
    ) {
        new StorageHandler(storageIP, username, password)
                .download(projectToDownload, userID, downloadPath);
    }
}
