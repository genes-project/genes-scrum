package org.genes.tools;

import com.github.tuupertunut.powershelllibjava.PowerShell;
import com.github.tuupertunut.powershelllibjava.PowerShellExecutionException;

import java.io.File;
import java.io.IOException;

public class Sender implements Runnable {

    String remoteHost;
    String remoteHostDestination;
    String remoteUser;
    String remotePassword;
    File fileToSend;

    public Sender(String remoteHost, String remoteHostDestination, String remoteUser, String remotePassword, File fileToSend) {
        this.remoteHost = remoteHost;
        this.remoteHostDestination = remoteHostDestination;
        this.remoteUser = remoteUser;
        this.remotePassword = remotePassword;
        this.fileToSend = fileToSend;
    }

    @Override
    public void run() {
        try (PowerShell psSession = PowerShell.open()) {
            System.out.println("Sending " + fileToSend.getAbsolutePath() + " to " + remoteHost + " at " + remoteHostDestination + "...");
            System.out.println(psSession.executeCommands(
                    "$cred = New-Object System.Management.Automation.PSCredential (\"" + remoteUser + "\", (ConvertTo-SecureString \"" + remotePassword + "\" -AsPlainText -Force))",
                    "$session = New-PSSession -Credential $cred -ComputerName " + remoteHost,
                    "Copy-Item -Path \"" + fileToSend.getAbsolutePath() + "\" -Destination \"" + remoteHostDestination + "\" -ToSession $session",
                    "Remove-PSSession $session"
            ));
            System.out.println("File saved at " + remoteHost + " under " + remoteHostDestination + "\\" + fileToSend.getName());
        } catch (IOException | PowerShellExecutionException ex) {
            ex.printStackTrace();
        }
    }
}
