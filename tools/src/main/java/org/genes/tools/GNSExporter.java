package org.genes.tools;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.stream.Collectors;
import com.jsoniter.JsonIterator;
import com.jsoniter.any.Any;

public class GNSExporter implements Callable<File> {

    private final String server;
    private final String projectName;
    private final File fileToExport;

    public GNSExporter(String server, String projectName, File fileToExport) {
        this.server = server;
        this.projectName = projectName;
        this.fileToExport = fileToExport;
    }

    @Override
    public File call() {
        // Checking if given file is not a directory
        if (fileToExport.isDirectory()) {
            System.out.println("Given file is a directory!");
            System.exit(1);
        }

        // Generating HTTP request URL
        System.out.println("Connected to: " + server);
        String serverURL = "http://" + server + "/v2/";
        String projectID = getProjectID(projectName, serverURL);
        String exportURL = serverURL + "projects/" + projectID + "/export?include_images=yes&include_snapshots=yes&reset_mac_addresses=no&compression=zip";

        // Exporting and saving project
        System.out.println("Exporting project \"" + projectName + "\" with ID \"" + projectID + "\"\n");
        export(exportURL, fileToExport);
        System.out.println("\nSaved exported file to \"" + fileToExport.getAbsolutePath() + "\"");

        return fileToExport;
    }

    // Gets given project ID from GNS3-Server
    private String getProjectID(String name, String serverURL) {
        String result = getProjects(serverURL).get(name);

        if(result != null) {
            return result;
        } else {
            System.out.println("Projekt mit dem Namen " + name + " wurde nicht gefunden!");
            System.exit(1);
            return null;
        }
    }

    // Gets all projects and returns them with ID and name
    private Map<String, String> getProjects(String serverURL) {
        Any projects = sendAndReceive(serverURL + "projects");

        assert projects != null;
        return projects.asList().stream()
                .collect(Collectors.toMap(e -> e.toString("name"), e -> e.toString("project_id")));
    }

    // Creates a HTTP connection with necessary settings, needed for InputStream
    private HttpURLConnection createHttp(String request) {
        URL url;
        HttpURLConnection http = null;
        try {
            url = new URL(request);
            http = (HttpURLConnection) url.openConnection();
            http.setRequestMethod("GET");
            http.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            http.setRequestProperty("Accept", "application/json");
            http.setDoOutput(true);
            http.connect();
        } catch (IOException ex) {
            System.out.println("Could not read response: " + ex.getMessage());
            System.exit(1);
        }
        return http;
    }

    // Sends a HTTP request to the given URL and returns a JSON object
    private Any sendAndReceive(String request) {
        HttpURLConnection http = createHttp(request);
        StringBuilder response = new StringBuilder();
        try {
            try(InputStream is = http.getInputStream(); InputStreamReader bis = new InputStreamReader(is); BufferedReader br = new BufferedReader(bis)) {
                int character;
                while((character = br.read()) != -1) {
                    response.append((char) character);
                }
            } catch (IOException ex) {
                System.out.println("Could not read response: " + ex.getMessage());
                throw new IOException();
            }
        } catch (IOException e) {
            printErrorGNS(http);
        }
        if(response.length() > 0) {
            return JsonIterator.deserialize(response.toString());
        } else {
            return null;
        }
    }

    // Sends a HTTP request to a given URL (should be the export call of GNS3), receives the binary file and saves it
    private void export(String request, File fileName) {
        HttpURLConnection http = createHttp(request);
        try (FileOutputStream fileOutputStream = new FileOutputStream(fileName); ReadableByteChannel readableByteChannel = Channels.newChannel(http.getInputStream())) {
            int factor = 1024 * 1024;
            long readCount = (5 * factor);
            long read = readCount;
            int counter = 0;
            String line = "";
            while(readCount == read) {
                read = fileOutputStream.getChannel()
                        .transferFrom(readableByteChannel, readCount*counter, readCount);
                counter++;
                deleteLine(line);
                line = "Read " + String.format("%.2f", (counter * readCount + 0.0)/factor) + "MB";
                System.out.print(line);
            }
            deleteLine(line);
            line = "Read " + String.format("%.2f", ((counter-1) * readCount + read + 0.0)/factor) + "MB";
            System.out.println(line);
        } catch (IOException e) {
            e.printStackTrace();
            printErrorGNS(http);
        }
    }

    // Deletes a line in the console
    private void deleteLine(String line) {
        for (int i = 0; i < line.length(); i++) {
            System.out.print("\b");
        }
    }

    // Prints the HTTP error message from GNS3 and exits the program
    private void printErrorGNS(HttpURLConnection http) {
        String errorMessage = "";
        if(http.getErrorStream() != null)
            errorMessage = (new BufferedReader(
                    new InputStreamReader(http.getErrorStream(), StandardCharsets.UTF_8)
            ).lines()
                    .collect(Collectors.joining("\n"))
            );
        System.out.println("Could not establish a connection to server:\n" + errorMessage);
        System.exit(1);
    }
}
