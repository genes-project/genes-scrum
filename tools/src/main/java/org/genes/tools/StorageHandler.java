package org.genes.tools;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.function.Consumer;

public class StorageHandler {

    private final String storageIP, username, password;

    public StorageHandler(String storageIP, String username, String password) {
        this.storageIP = storageIP;
        this.username = username;
        this.password = password;
    }

    public void download(Path projectToDownload, int userID, Path destination) {
        useClient(ftpClient ->
                withOutputStream(destination, fileOutputStream -> {
                    try {
                        ftpClient.retrieveFile(userID + "/" + projectToDownload.getFileName().toString(), fileOutputStream);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }));
    }

    public void upload(Path source, int userID) {
        useClient(ftpClient ->
                withInputStream(source, fileInputStream -> {
                    try {
                        boolean dirExists = ftpClient.changeWorkingDirectory(String.valueOf(userID));
                        if(!dirExists) {
                            ftpClient.makeDirectory(String.valueOf(userID));
                            ftpClient.changeWorkingDirectory(String.valueOf(userID));
                        }
                        ftpClient.storeFile(source.getFileName().toString(), fileInputStream);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }));
    }

    public boolean projectExists(String topologyName, int id) {
        AtomicBoolean result = new AtomicBoolean(false);
        useClient(ftpClient -> {
            try {
                result.set(ftpClient.listFiles(id + "/" + topologyName).length > 0);
            } catch (IOException e) {
                result.set(false);
            }
        });
        return result.get();
    }

    private void useClient(Consumer<FTPClient> consumer) {
        FTPClient client = new FTPClient();
        try {
            client.connect(storageIP);
            client.login(username, password);
            client.enterLocalPassiveMode();
            client.setFileType(FTP.BINARY_FILE_TYPE);
            consumer.accept(client);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                client.logout();
                client.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void withInputStream(Path path, Consumer<FileInputStream> consumer) {
        try(FileInputStream fis = new FileInputStream(path.toFile())) {
            consumer.accept(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void withOutputStream(Path path, Consumer<FileOutputStream> consumer) {
        try(FileOutputStream fos = new FileOutputStream(path.toFile())) {
            consumer.accept(fos);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
