package org.genes.tools;

import com.github.tuupertunut.powershelllibjava.PowerShell;
import com.github.tuupertunut.powershelllibjava.PowerShellExecutionException;

import java.io.IOException;
import java.nio.file.Path;

public class Starter implements Runnable {

    private final String username, password, computerName, gns3exe;
    private final Path gns3exportableProjectPath;

    public Starter(String username, String password, String computerName, String gns3exe, Path gns3exportableProjectPath) {
        this.username = username;
        this.password = password;
        this.computerName = computerName;
        this.gns3exe = gns3exe;
        this.gns3exportableProjectPath = gns3exportableProjectPath;
    }

    @Override
    public void run() {
        try(PowerShell ps = PowerShell.open()) {
            String commandToExecute = String.format("\"%s\" %s", gns3exe, gns3exportableProjectPath);
            String invokeCommand =
                    String.format("psexec -accepteula -i 1 -d -u %s -p %s \\\\%s %s", username, password, computerName, commandToExecute);
            System.out.println(invokeCommand);
            String output = ps.executeCommands(invokeCommand);
            System.out.println(output);
        } catch (IOException | PowerShellExecutionException ex) {
            ex.printStackTrace();
        }
    }
}
