package org.genes.tools;

import org.junit.jupiter.api.Test;

import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.*;

public class StorageHandlerTest {

    @Test
    void projectExists_test() {
        StorageHandler sut = new StorageHandler
                ( "ftp.dlptest.com"
                , "dlpuser"
                , "rNrKYTX9g7z3RgJRmxWuGHbeu");
        assertFalse(sut.projectExists("topology_test", 0));
        sut.upload(Paths.get("topology_test"), 0);
        assertTrue(sut.projectExists("topology_test", 0));
    }
}
